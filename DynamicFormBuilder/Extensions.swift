//
//  Extensions.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 30/03/2022.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UIViewController {
    
    public var message: Binder<String> {
        return Binder.init(self.base, binding: { (vc, message) in
            vc.showBindedError(message: message)
        })
    }
}

extension UIViewController {
    
    private struct AssociatedKeys {
        static var loadingView = "LoadingViewAssociatedKey"
    }
    
    public var loadingView: UIView? {
        get{
            return objc_getAssociatedObject(self, &AssociatedKeys.loadingView) as? UIView
        }
        set{
            objc_setAssociatedObject(self, &AssociatedKeys.loadingView, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public func showBindedError(message: String) {
        let alertC = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok button action used with alert view"), style: .cancel, handler: nil)
        alertC.addAction(okAction)
        
        present(alertC, animated: true, completion: nil)
    }
}
