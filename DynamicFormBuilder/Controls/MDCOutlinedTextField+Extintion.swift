//
//  MDCOutlinedTextField+Extintion.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 15/01/2023.
//

import Foundation
import MaterialComponents.MDCOutlinedTextField

extension MDCOutlinedTextField {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }else if action == #selector(UIResponderStandardEditActions.cut(_:)) {
            return false
        } else if action == #selector(UIResponderStandardEditActions.copy(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
