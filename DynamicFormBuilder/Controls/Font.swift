//
//  Font.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 01/06/2022.
//

import Foundation
import UIKit

@objc public class Font: NSObject{
    
    public static let shared = Font()
    
    public var font: FontModel? = FontModel(secondaryFont: UIFont.systemFont(ofSize: 17), primaryFont: UIFont.systemFont(ofSize: 13), textFieldFont: UIFont.systemFont(ofSize: 11), helperFont: UIFont.systemFont(ofSize: 11), textButtonFont: UIFont.systemFont(ofSize: 11))
    
    fileprivate override init(){
        
    }
    
}

public class FontModel {
    
    public var secondaryFont: UIFont
    public var primaryFont: UIFont
    public var textFieldFont: UIFont
    public var helperFont: UIFont
    public var textButtonFont: UIFont
    
    public init(secondaryFont: UIFont, primaryFont: UIFont, textFieldFont: UIFont, helperFont: UIFont, textButtonFont:  UIFont) {
        
        self.secondaryFont = secondaryFont
        self.primaryFont = primaryFont
        self.textFieldFont = textFieldFont
        self.helperFont = helperFont
        self.textButtonFont = textButtonFont
    }
}
