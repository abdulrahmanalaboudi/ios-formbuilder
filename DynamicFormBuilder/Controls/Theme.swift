//
//  Theme.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 10/03/2022.
//

import Foundation
import UIKit

@objc public class Theme: NSObject{
    
    public static let shared = Theme()
    
    public var theme: ThemeModel? = ThemeModel(primaryColor: .black, secondaryColor: .white, textColor: .black, textColorOnEditing: .black, secondaryTextColor: .gray, requiredColor: .red, buttonTextColor: .white, backgroundColor: .systemYellow, onSwitchColor: .red, offSwitchColor: .gray, helperLabelColorOnDisabled: .black, helperLabelColorOnEditing: .systemBlue, helperLabelColorOnNormal: .black, outlineColorOnNormal: .black, outlineColorOnEditing: .systemBlue, outlineColorOnDisabled: .black)
    
    fileprivate override init(){
        
    }
    
}

public class ThemeModel {
    public var primaryColor: UIColor
    public var secondaryColor: UIColor
    public var textColor: UIColor
    public var textColorOnEditing: UIColor
    public var requiredColor: UIColor
    public var buttonTextColor: UIColor
    public var viewColor: UIColor
    public var onSwitchColor: UIColor
    public var offSwitchColor: UIColor
    public var helperLabelColorOnEditing: UIColor
    public var helperLabelColorOnDisabled: UIColor
    public var helperLabelColorOnNormal: UIColor
    public var outlineColorOnNormal: UIColor
    public var outlineColorOnEditing: UIColor
    public var outlineColorOnDisabled: UIColor
    public var secondaryTextColor: UIColor
    
    public init(primaryColor: UIColor, secondaryColor: UIColor, textColor: UIColor, textColorOnEditing: UIColor, secondaryTextColor: UIColor, requiredColor: UIColor, buttonTextColor: UIColor, backgroundColor: UIColor, onSwitchColor: UIColor, offSwitchColor: UIColor, helperLabelColorOnDisabled: UIColor, helperLabelColorOnEditing: UIColor, helperLabelColorOnNormal: UIColor, outlineColorOnNormal: UIColor, outlineColorOnEditing: UIColor, outlineColorOnDisabled: UIColor) {
        
        self.primaryColor = primaryColor
        self.secondaryColor = secondaryColor
        self.textColor = textColor
        self.textColorOnEditing = textColorOnEditing
        self.requiredColor = requiredColor
        self.buttonTextColor = buttonTextColor
        self.viewColor = backgroundColor
        self.onSwitchColor = onSwitchColor
        self.offSwitchColor = offSwitchColor
        self.helperLabelColorOnEditing = helperLabelColorOnEditing
        self.helperLabelColorOnNormal = helperLabelColorOnNormal
        self.helperLabelColorOnDisabled = helperLabelColorOnDisabled
        self.outlineColorOnNormal = outlineColorOnNormal
        self.outlineColorOnEditing = outlineColorOnEditing
        self.outlineColorOnDisabled = outlineColorOnDisabled
        self.secondaryTextColor = secondaryTextColor
        
    }
}
