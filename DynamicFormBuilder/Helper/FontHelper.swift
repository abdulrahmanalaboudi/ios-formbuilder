//
//  FontHelper.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 31/05/2022.
//

import Foundation
import UIKit

public class FontHelper{
    
    @discardableResult
    public func installFont(identifier: String, forResource: String, withExtension: String) -> Bool {
        
        let bundle = Bundle(identifier: identifier)
        let bundelURL = (bundle?.url(forResource: forResource, withExtension: withExtension))!

        let fontData = try! Data(contentsOf: bundelURL)
        
        if let provider = CGDataProvider.init(data: fontData as CFData) {
            
            var error: Unmanaged<CFError>?
            
            let font:CGFont = CGFont(provider)!
            if (!CTFontManagerRegisterGraphicsFont(font, &error)) {
                print(error.debugDescription)
                return false
            } else {
                return true
            }
        }
        return false
    }
}
