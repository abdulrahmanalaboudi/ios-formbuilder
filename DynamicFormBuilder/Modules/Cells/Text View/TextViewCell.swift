//
//  TextViewCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 08/05/2022.
//

import Foundation
import UIKit
import RxSwift

class TextViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func setupText(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> TextViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewCell", for: indexPath) as! TextViewCell
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        cell.titleLabel.text = element.label
        cell.setupTheme()
        cell.setupFont()
        cell.selectionStyle = .none
        
        return cell
    }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        titleLabel.textColor = theme?.textColor

    }
    
    func setupFont(){
        
        let font  = Font.shared.font
        
        titleLabel.font = font?.primaryFont
        
    }
}
