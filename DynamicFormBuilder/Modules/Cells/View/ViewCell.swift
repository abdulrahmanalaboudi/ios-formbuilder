//
//  ViewCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 08/05/2022.
//

import Foundation
import UIKit
import RxSwift

class ViewCell: UITableViewCell {
    
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func setupView(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> ViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewCell", for: indexPath) as! ViewCell
        cell.myImageView.image = element.leftImageView ?? UIImage()
        cell.myView.layer.cornerRadius = element.cornerRadius ?? 0
        cell.titleLabel.text = element.label
        cell.selectionStyle = .none
        cell.setupTheme()
        cell.setupFont()
        cell.titleLabel.font = UIFont(name: "COMICATE", size: 30)
        
        return cell
    }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        myView.backgroundColor = theme?.viewColor
        titleLabel.textColor = theme?.secondaryColor
        myImageView.tintColor = theme?.secondaryColor
    }
    
    func setupFont(){
        
        let font  = Font.shared.font
        
        titleLabel.font = font?.primaryFont
        
    }
}
