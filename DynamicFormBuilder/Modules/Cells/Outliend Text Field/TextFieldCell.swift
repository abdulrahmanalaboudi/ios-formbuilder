//
//  TextFieldCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 24/02/2022.
//

import Foundation
import UIKit
import RxSwift
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class TextFieldCell: UITableViewCell {
    
    @IBOutlet weak var outlinedTextField: MDCOutlinedTextField!
    
    var disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func setupText(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> TextFieldCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
        
        cell.outlinedTextField.label.text = element.label ?? ""
        cell.outlinedTextField.placeholder = element.placeholder
        cell.setupTheme()
        cell.setupFont()
        cell.selectionStyle = .none
        cell.outlinedTextField.sizeToFit()
        
        if element.isValid == false && (element.fieldRequired == true){
            
            cell.outlinedTextField.setOutlineColor(.red, for: .normal)
            cell.outlinedTextField.setOutlineColor(.red, for: .editing)
            cell.outlinedTextField.setOutlineColor(.red , for: .disabled)
            cell.outlinedTextField.leadingAssistiveLabel.text = element.helperLabel

        } else {
            let theme  = Theme.shared.theme
            cell.outlinedTextField.leadingAssistiveLabel.text = nil
            cell.outlinedTextField.setOutlineColor(theme!.outlineColorOnNormal, for: .normal)
            cell.outlinedTextField.setOutlineColor(theme!.outlineColorOnEditing, for: .editing)
            cell.outlinedTextField.setOutlineColor(theme!.outlineColorOnDisabled , for: .disabled)
        }

        return cell
    }
    
    func setupTheme(){
        let theme  = Theme.shared.theme

        outlinedTextField.setOutlineColor(theme!.outlineColorOnNormal, for: .normal)
        outlinedTextField.setOutlineColor(theme!.outlineColorOnEditing, for: .editing)
        outlinedTextField.setOutlineColor(theme!.outlineColorOnDisabled , for: .disabled)

        outlinedTextField.setTextColor(theme!.textColorOnEditing, for: .editing)
        outlinedTextField.setTextColor(theme!.textColor, for: .disabled)
        outlinedTextField.setTextColor(theme!.textColor, for: .normal)
        
        outlinedTextField.tintColor = theme?.primaryColor // pointer color

        outlinedTextField.setNormalLabelColor(theme!.textColorOnEditing, for: .editing) // label color
        outlinedTextField.setNormalLabelColor(theme!.textColor, for: .disabled) // label color
        outlinedTextField.setNormalLabelColor(theme!.textColor, for: .normal) // label color
        
        outlinedTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnNormal, for: .normal) // helper color
        outlinedTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnEditing, for: .editing) // helper color
        outlinedTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnDisabled, for: .disabled) // helper color
    }
    
    func setupFont(){
        
        let font  = Font.shared.font

        outlinedTextField.font  = font!.textFieldFont
        outlinedTextField.leadingAssistiveLabel.font = font!.helperFont
    }
}
