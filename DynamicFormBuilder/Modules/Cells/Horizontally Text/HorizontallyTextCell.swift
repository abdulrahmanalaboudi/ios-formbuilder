//
//  HorizontallyTextCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 09/05/2022.
//

import Foundation
import UIKit
import RxSwift

class HorizontallyTextCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    static func setupHorizontallyText(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> HorizontallyTextCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HorizontallyTextCell", for: indexPath) as! HorizontallyTextCell
        cell.titleLabel.text = element.label
        cell.secondLabel.text = element.secondaryLabel
        cell.selectionStyle = .none
        cell .setupTheme()
        cell.setupFont()
        return cell
    }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        titleLabel.textColor = theme?.primaryColor
        secondLabel.textColor = theme?.secondaryTextColor
    }
    
    func setupFont(){
        
        let font  = Font.shared.font

        titleLabel.font = font?.primaryFont
        secondLabel.font = font?.secondaryFont
    }
}
