//
//  NormalTextFieldCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 12/05/2022.
//

import Foundation
import UIKit
import RxSwift
import MaterialComponents.MDCFilledTextField

class NormalTextFieldCell: UITableViewCell {
    
    @IBOutlet weak var filledTextField: MDCFilledTextField!
    
    var disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func setupText(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> NormalTextFieldCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "NormalTextFieldCell", for: indexPath) as! NormalTextFieldCell
        
        cell.filledTextField.label.text = element.label ?? ""
        cell.filledTextField.placeholder  = element.placeholder
        cell.filledTextField.leadingAssistiveLabel.text  = element.helperLabel
        cell.filledTextField.sizeToFit()
        cell.setupTheme()
        cell.setupFont()
        cell.selectionStyle = .none
        return cell
    }
    
    func setupTheme(){
        let theme  = Theme.shared.theme
        
        
        filledTextField.setTextColor(theme!.textColorOnEditing, for: .editing)
        filledTextField.setTextColor(theme!.textColor, for: .disabled)
        filledTextField.setTextColor(theme!.textColor, for: .normal)
        
        filledTextField.tintColor = theme!.textColor // pointer color
        
        filledTextField.setNormalLabelColor(theme!.textColorOnEditing, for: .editing) // label color
        filledTextField.setNormalLabelColor(theme!.textColor, for: .disabled) // label color
        filledTextField.setNormalLabelColor(theme!.textColor, for: .normal) // label color
        
        filledTextField.setFloatingLabelColor(theme!.textColor, for: .normal)
        filledTextField.setFloatingLabelColor(theme!.textColorOnEditing, for: .editing)
        filledTextField.setFloatingLabelColor(theme!.textColor, for: .disabled)
        
        filledTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnNormal, for: .normal) // helper color
        filledTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnEditing, for: .editing) // helper color
        filledTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnDisabled, for: .disabled) // helper color
    }
    
    func setupFont(){
        
        let font  = Font.shared.font

        filledTextField.font  = font!.textFieldFont
        filledTextField.leadingAssistiveLabel.font = font!.helperFont
    }
}
