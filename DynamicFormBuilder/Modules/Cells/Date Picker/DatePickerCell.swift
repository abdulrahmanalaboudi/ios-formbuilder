//
//  DatePickerCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 06/03/2022.
//


import Foundation
import UIKit
import RxSwift
import MaterialComponents.MDCOutlinedTextField

class DatePickerCell: UITableViewCell {
    
    @IBOutlet var datePickerTextField: MDCOutlinedTextField!
    @IBOutlet weak var datePickerImage: UIImageView!
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func setupDate(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> DatePickerCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell", for: indexPath) as! DatePickerCell
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        let dateString = dateFormatter.string(from: element.dateUserInput ?? Date())
        cell.datePickerTextField.text = dateString
        cell.datePickerTextField.label.text = element.label ?? ""
        cell.datePickerTextField.placeholder  = element.placeholder
        cell.datePickerTextField.leadingAssistiveLabel.text  = element.helperLabel
        cell.datePickerImage.image = element.rightImageView
        cell.datePickerTextField.sizeToFit()
        cell.setupTheme()
        cell.setupFont()
        cell.selectionStyle = .none
        
        return cell
    }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        datePickerTextField.textColor = theme?.textColor
        
        datePickerTextField.setOutlineColor(theme!.outlineColorOnNormal, for: .normal)
        datePickerTextField.setOutlineColor(theme!.outlineColorOnEditing, for: .editing)
        datePickerTextField.setOutlineColor(theme!.outlineColorOnDisabled , for: .disabled)
        
        datePickerTextField.setTextColor(theme!.textColorOnEditing, for: .editing)
        datePickerTextField.setTextColor(theme!.textColor, for: .disabled)
        datePickerTextField.setTextColor(theme!.textColor, for: .normal)
        
        datePickerTextField.setNormalLabelColor(theme!.textColorOnEditing, for: .editing) // label color
        datePickerTextField.setNormalLabelColor(theme!.textColor, for: .disabled) // label color
        datePickerTextField.setNormalLabelColor(theme!.textColor, for: .normal) // label color
        
        datePickerTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnNormal, for: .normal) // helper color
        datePickerTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnEditing, for: .editing) // helper color
        datePickerTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnDisabled, for: .disabled) // helper color
        
    }
    
    func setupFont(){
        
        let font  = Font.shared.font
        
        datePickerTextField.font  = font!.textFieldFont
        datePickerTextField.leadingAssistiveLabel.font = font!.helperFont
    }
}
