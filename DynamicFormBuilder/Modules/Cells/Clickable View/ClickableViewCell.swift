//
//  ClickableViewCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 08/05/2022.
//

import Foundation
import UIKit
import RxSwift

class ClickableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        static func setupClickableView(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> ClickableViewCell{
    
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClickableViewCell", for: indexPath) as! ClickableViewCell
            cell.titleLabel.text = element.label
            cell.rightButton.setImage(element.rightImageView, for: .normal)
            cell.leftImageView.image = element.leftImageView
            cell.rightButton.setTitle("", for: .normal)
            cell.selectionStyle = .none
            cell .setupTheme()
            cell.setupFont()
            return cell
        }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        leftImageView.tintColor = theme?.primaryColor
        rightButton.tintColor = theme?.primaryColor
        titleLabel.textColor = theme?.primaryColor
    }
    
    func setupFont(){
        
        let font  = Font.shared.font

        titleLabel.font = font?.primaryFont
    }
}
