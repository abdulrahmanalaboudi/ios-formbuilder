//
//  SwitchCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 08/05/2022.
//

import Foundation
import UIKit
import RxSwift

class SwitchCell: UITableViewCell {
    
    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var titleLabel: UILabel!
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        static func setupSwitch(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> SwitchCell{
    
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchCell", for: indexPath) as! SwitchCell
            element.switchUserSelected = cell.mySwitch
            cell.titleLabel.text = element.label
            cell.selectionStyle = .none
            cell .setupTheme()
            cell.setupFont()
            return cell
        }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        mySwitch.onTintColor = theme?.onSwitchColor
        mySwitch.tintColor = theme?.offSwitchColor
        titleLabel.textColor = theme?.primaryColor
    }
    
    func setupFont(){
        
        let font  = Font.shared.font

        titleLabel.font = font?.primaryFont
    }
}
