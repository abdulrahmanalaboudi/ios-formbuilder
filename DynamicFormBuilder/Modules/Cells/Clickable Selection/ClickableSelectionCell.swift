//
//  ClickableSelectionCell.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 16/01/2023.
//

import Foundation
import UIKit
import RxSwift
import MaterialComponents.MDCOutlinedTextField

class ClickableSelectionCell: UITableViewCell {
    
    @IBOutlet var pickerTextField: MDCOutlinedTextField!
    @IBOutlet weak var pickerImage: UIImageView!
    @IBOutlet var clickableButton: UIButton!
    
    var pickerView = UIPickerView()
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
        pickerView = UIPickerView()
        pickerView.reloadAllComponents()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func setupClickableSelection(indexPath: IndexPath, tableView: UITableView,element: FieldModel) -> ClickableSelectionCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClickableSelectionCell", for: indexPath) as! ClickableSelectionCell
        
        cell.pickerTextField.label.text = element.label ?? ""
        cell.pickerTextField.placeholder  = element.placeholder
        cell.pickerImage.image = element.rightImageView
        cell.pickerTextField.text = element.lookupUserInput?.name ?? ""
        cell.pickerTextField.inputView = cell.pickerView
        cell.setupTheme()
        cell.setupFont()
        cell.pickerTextField.sizeToFit()
        cell.selectionStyle = .none
        
        if element.isValid == false && (element.fieldRequired == true){
            
            cell.pickerTextField.setOutlineColor(.red, for: .normal)
            cell.pickerTextField.setOutlineColor(.red, for: .editing)
            cell.pickerTextField.setOutlineColor(.red , for: .disabled)
            cell.pickerTextField.leadingAssistiveLabel.text = element.helperLabel
            
        } else {
            
            let theme  = Theme.shared.theme
            cell.pickerTextField.leadingAssistiveLabel.text = nil
            cell.pickerTextField.setOutlineColor(theme!.outlineColorOnNormal, for: .normal)
            cell.pickerTextField.setOutlineColor(theme!.outlineColorOnEditing, for: .editing)
            cell.pickerTextField.setOutlineColor(theme!.outlineColorOnDisabled , for: .disabled)
            
        }
        return cell
    }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        
        pickerImage.tintColor = theme?.primaryColor
        
        pickerTextField.setTextColor(theme!.textColorOnEditing, for: .editing)
        pickerTextField.setTextColor(theme!.textColor, for: .disabled)
        pickerTextField.setTextColor(theme!.textColor, for: .normal)
        
        pickerTextField.setNormalLabelColor(theme!.textColorOnEditing, for: .editing) // label color
        pickerTextField.setNormalLabelColor(theme!.textColor, for: .disabled) // label color
        pickerTextField.setNormalLabelColor(theme!.textColor, for: .normal) // label color
        
        pickerTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnNormal, for: .normal) // helper color
        pickerTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnEditing, for: .editing) // helper color
        pickerTextField.setLeadingAssistiveLabelColor(theme!.helperLabelColorOnDisabled, for: .disabled) // helper color
        
    }
    
    func setupFont(){
        
        let font  = Font.shared.font
        
        pickerTextField.font  = font!.textFieldFont
        pickerTextField.leadingAssistiveLabel.font = font!.helperFont
    }
}
