//
//  InitialVC.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 13/03/2022.
//

import UIKit

class InitialVC: UIViewController {
    
    var formVC: FormFieldVC!
    var fontHelper = FontHelper()
    var themeHelper = Theme.shared.theme
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        if (fontHelper.installFont(identifier: "com.LeadingPoint.DynamicFormBuilder", forResource: "COMICATE", withExtension: "TTF")) {
        //            print("Font installed")
        //        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        formVC = storyboard.instantiateViewController(withIdentifier: "FormFieldVC") as? FormFieldVC
        
        let formVM = FormFieldVM(fieldsModels: [
            
            FieldModel(name: "label",
                       label: "(3/3) Further Information",
                       type: .text,
                       lockups: nil,
                       fieldRequired: false,
                       stringUserInput: ""),
            
            FieldModel(name: "warrning",
                       label: "Please .",
                       type: .view,
                       lockups: nil,
                       fieldRequired: false,
                       stringUserInput: "",
                       leftImageView: nil,
                       cornerRadius: 5.0),
            
            FieldModel(name: "warrning",
                       label: "This is clickable view",
                       type: .clickableView,
                       lockups: nil,
                       fieldRequired: false,
                       stringUserInput: "",
                       leftImageView: UIImage(named: ""),
                       cornerRadius: 5.0),
            
            FieldModel(name: "Nationality",
                       label: "Receiver Nationality",
                       helperLabel:  "shahim",
                       placeholder: "Please enter your Nationality",
                       type: .selection,
                       lockups:([LookupArrayModel(key: "testNationality", name: "testNationality"),
                                 LookupArrayModel(key: "22", name: "22")]),
                       fieldRequired: true,
                       stringUserInput: nil,
                       lookupUserInput: LookupArrayModel(key: "testNationality", name: "testNationality"),
                       rightImageView: UIImage(named: "imgpsh_mobile_save")),
            
            FieldModel(name: "Clickable Selection",
                       label: "Clickable Selection",
                       helperLabel:  "shahim",
                       placeholder: "Please Select your Clickable Selection",
                       type: .clickableSelection,
                       lockups:([LookupArrayModel(key: "testNationality", name: "testNationality"),
                                 LookupArrayModel(key: "22", name: "22")]),
                       fieldRequired: true,
                       stringUserInput: nil,
                       lookupUserInput: LookupArrayModel(key: "testNationality", name: "testNationality"),
                       rightImageView: UIImage(named: "imgpsh_mobile_save")),
            
            FieldModel(name: "Address",
                       label: "Receiver Address",
                       helperLabel:  "Abu shahim",
                       placeholder: "Please enter your Address",
                       type: .float,
                       lockups: nil,
                       fieldRequired: true,
                       stringUserInput: nil,
                       maxLength: 3),
            
            FieldModel(name: "Relationship",
                       label: "Relationship",
                       helperLabel:  "helperLabellaf",
                       placeholder: "Please select your Relationship",
                       type: .selection,
                       lockups:([LookupArrayModel(key: "Relationship", name: "Relationship")]),
                       fieldRequired: true,
                       stringUserInput: nil,
                       lookupUserInput: nil,
                       rightImageView: UIImage(named: "imgpsh_mobile_save")),
            
            FieldModel(name: "Purpose",
                       label: "Purpose of Transfer",
                       helperLabel:  "helperLabel;;;;;",
                       placeholder: "Please select your Purpose of Transfer",
                       type: .selection,
                       lockups:([LookupArrayModel(key: "Purpose", name: "Purpose"),LookupArrayModel(key: "Purpose2", name: "Purpose2")]),
                       fieldRequired: false,
                       stringUserInput: nil,
                       lookupUserInput: nil,
                       rightImageView: UIImage(named: "imgpsh_mobile_save"))
        ])
        
        Theme.shared.theme = ThemeModel(primaryColor: .black, secondaryColor: .yellow, textColor: .black, textColorOnEditing: .black, secondaryTextColor: .black, requiredColor: .black, buttonTextColor: .black, backgroundColor: .black, onSwitchColor: .black, offSwitchColor: .black, helperLabelColorOnDisabled: .black, helperLabelColorOnEditing: .red, helperLabelColorOnNormal: .red, outlineColorOnNormal: .black, outlineColorOnEditing: .black, outlineColorOnDisabled: .black)
        
        
        
        //        let formVM = FormFieldVM(fieldsModels: [FieldModel(name: "warrning",
        //                                                           label: "Please use the exact name shown on the beneficiary ID to be able to get the money from the bank.",
        //                                                           type: .view,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: "",
        //                                                           leftImageView: UIImage(systemName: "info"),
        //                                                           cornerRadius: 5.0),
        //                                                FieldModel(name: "First Name",
        //                                                           label: "First Name",
        //                                                           secondaryLabel: "Please enter your first name",
        //                                                           helperLabel:  "First name heper",
        //                                                           placeholder: "Please enter your first name",
        //                                                           type:.outliendTextField,
        //                                                           lockups:([LookupArrayModel(key: "test", name: "test")]),
        //                                                           fieldRequired: true, stringUserInput: "str"),
        //                                                FieldModel(name: "Last Name",
        //                                                           label: "Last Name",
        //                                                           secondaryLabel: "Please enter your last name",
        //                                                           helperLabel:  "Last name heper",
        //                                                           placeholder: "Please enter your last name",
        //                                                           type:.normalTextField,
        //                                                           lockups:([LookupArrayModel(key: "test", name: "test")]),
        //                                                           fieldRequired: true, stringUserInput: "str"),
        //                                                FieldModel(name: "label",
        //                                                           label: "This is the norma label",
        //                                                           type: .text,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: ""),
        //                                                FieldModel(name: "conditions",
        //                                                           label: "Conditions",
        //                                                           type: .switchView,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: ""),
        //                                                FieldModel( name: "cickableView",
        //                                                            label: "Cickable View",
        //                                                            type: .clickableView,
        //                                                            lockups: nil,
        //                                                            fieldRequired: false,
        //                                                            stringUserInput: nil,
        //                                                            leftImageView: UIImage(systemName: "pencil"),
        //                                                            rightImageView: UIImage(systemName: "pencil")),
        //                                                FieldModel(name: "c",
        //                                                           label: "c",
        //                                                           type: .selection,
        //
        //                                                           lockups:([LookupArrayModel(key: "test", name: "test")]),
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: nil,
        //                                                           lookupUserInput: LookupArrayModel(key: "test", name: "test")),
        //                                                FieldModel(name: "Email",
        //                                                           label: "Select ....",
        //                                                           type: .selection,
        //                                                           lockups:([LookupArrayModel(key: "test", name: "test")]),
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: ""),
        //                                                FieldModel(name: "BOD",
        //                                                           label: "BOD",
        //                                                           type: .date,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: nil,
        //                                                           dateUserInput: nil,
        //                                                           rightImageView: UIImage(systemName: "calender")),
        //                                                FieldModel(name: "Gender",
        //                                                           label: "Gender",
        //                                                           type: .date,
        //                                                           lockups:nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: ""),
        //                                                FieldModel(name: "Camera",
        //                                                           label: "Camera",
        //                                                           type: .attachement,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: "",
        //                                                           cornerRadius: 5.0),
        //                                                FieldModel(name: "Gallery",
        //                                                           label: "Gallery",
        //                                                           type: .date,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: ""),
        //                                                FieldModel(name: "Gallery",
        //                                                           label: "First Label",
        //                                                           secondaryLabel: "Second Label",
        //                                                           type: .horizontallyText,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: ""),
        //                                                FieldModel(name: "Gallery",
        //                                                           label: "First Label",
        //                                                           secondaryLabel: "Second Label",
        //                                                           type: .horizontallyText,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: ""),
        //                                                FieldModel(name: "Gallery",
        //                                                           label: "First Label",
        //                                                           secondaryLabel: "Second Label",
        //                                                           type: .horizontallyText,
        //                                                           lockups: nil,
        //                                                           fieldRequired: false,
        //                                                           stringUserInput: "")
        //                                               ])
        
        formVC.viewModel = formVM
        formVC.delegate = self
        
        self.present(formVC, animated: true, completion: nil)
    }
    
}

extension InitialVC: FormFieldVCDelegate{
    func clickableSelectionTapped(index: Int) {
        print(index)
    }
    
    func selectedSelection(key: String, name: String, index: Int) {
        print(key)
        print(name)
        print(index)
    }
    
    
    func showAlertWithBack(rootViewController: UIViewController, message: String, title: String, buttonTitle: String) {
        
    }
    
    func componentCallBack(rootViewController: UIViewController, field: FieldModel) {
        print("componentCallBack: \(String(describing: field.label))")
    }
    
    func didFinishFormFiling(rootViewController: UIViewController, fields: [FieldModel]) {
        formVC.dismiss(animated: true) {
            print(fields)
            print("didFinishFormFiling")
        }
    }
    
    func showAlert(rootViewController: UIViewController, message: String, title: String, buttonTitle: String) {
        
    }
    
}
