//
//  FormFieldVC.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 24/02/2022.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

@objc public protocol FormFieldVCDelegate: NSObjectProtocol {
    func showAlert(rootViewController: UIViewController, message: String, title: String, buttonTitle: String)
    func didFinishFormFiling(rootViewController: UIViewController, fields: [FieldModel])
    func showAlertWithBack(rootViewController: UIViewController, message: String, title: String, buttonTitle: String)
    func componentCallBack(rootViewController: UIViewController, field: FieldModel)
    func selectedSelection(key: String, name: String, index: Int)
    func clickableSelectionTapped(index: Int)
}

public class FormFieldVC: UIViewController {
    
    @IBOutlet private(set) var tableView: UITableView!
    @IBOutlet private(set) var submitButton: UIButton!
    
    public var viewModel: FormFieldVM!
    private let disposeBag = DisposeBag()
    public var delegate: FormFieldVCDelegate?
    let theme  = Theme.shared.theme
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupBindings()
        setupTheme()
        setupFont()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func setupTableView(){
        tableView.separatorColor = .clear
        
        tableView.register(UINib(nibName: "TextFieldNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "PickerNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "PickerCell")
        tableView.register(UINib(nibName: "DatePickerNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "DatePickerCell")
        tableView.register(UINib(nibName: "FileNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "FileCell")
        tableView.register(UINib(nibName: "TextViewNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "TextViewCell")
        tableView.register(UINib(nibName: "ViewNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "ViewCell")
        tableView.register(UINib(nibName: "SwitchNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "SwitchCell")
        tableView.register(UINib(nibName: "NormalTextFieldNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "NormalTextFieldCell")
        tableView.register(UINib(nibName: "ClickableViewNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "ClickableViewCell")
        tableView.register(UINib(nibName: "HorizontallyTextNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "HorizontallyTextCell")
        tableView.register(UINib(nibName: "ClickableSelectionNib", bundle: Bundle(for: FormFieldVC.self)), forCellReuseIdentifier: "ClickableSelectionCell")

        tableView.delegate = self
    }
    
    func setupBindings(){
        
        viewModel.isFinishFormFilling
            .subscribe(
                onNext:{ [weak self] isFinish in
                    guard let self = self else { return }
                    self.delegate?.didFinishFormFiling(rootViewController: self, fields: self.viewModel.fieldsArray)
                })
            .disposed(by: disposeBag)
        
        viewModel.errorMessage
            .subscribe(
                onNext:{ [weak self] message in
                    guard let self = self else { return }
                    self.delegate?.showAlert(rootViewController: self, message: "message", title: "title", buttonTitle: "OK")
                    
                })
            .disposed(by: disposeBag)
        
        viewModel.errorMessageWithBack
            .subscribe(
                onNext:{ [weak self] message in
                    guard let self = self else { return }
                    self.delegate?.showAlertWithBack(rootViewController: self, message: "message", title: "title", buttonTitle: "OK")
                    
                })
            .disposed(by: disposeBag)
        viewModel.fieldsModelsObs
            .asDriver(onErrorJustReturn: [FieldModel]())
            .drive(tableView.rx.items) {
                
                (tableView: UITableView, index: Int, element: FieldModel) in
                let indexPath = IndexPath(item: index, section: 0)
                let type = element.type
                
                switch type {
                    
                case .float:
                    let cell = TextFieldCell.setupText(indexPath: indexPath, tableView: tableView, element: element)
                    cell.outlinedTextField
                        .rx.textInput.text.orEmpty
                        .map{ ($0, element) }
                        .bind(to: self.viewModel.textChangedObs)
                        .disposed(by: cell.disposeBag)
                    cell.outlinedTextField.keyboardType = .decimalPad
                    
                    cell.outlinedTextField.rx.controlEvent([.editingDidEnd, .editingChanged])
                        .map{ ($0, element) }
                        .asObservable().subscribe({ [weak self] obs in
                            guard let self = self else { return }
                            
                            if self.viewModel.isValidFloat(text: cell.outlinedTextField.text , regex: obs.element?.1.regex ?? "^[a-zA-Z0-9._-]{1,30}$") {
                                print("done")
                                
                                cell.outlinedTextField.setOutlineColor(self.theme!.outlineColorOnNormal, for: .normal)
                                cell.outlinedTextField.setOutlineColor(self.theme!.outlineColorOnEditing, for: .editing)
                                cell.outlinedTextField.setOutlineColor(self.theme!.outlineColorOnDisabled , for: .disabled)
                                cell.outlinedTextField.leadingAssistiveLabel.text = nil
                                
                            }else{
                                print("ma done")
                                cell.outlinedTextField.leadingAssistiveLabel.text = obs.element?.1.helperLabel
                                
                            }
                        }).disposed(by: cell.disposeBag)
                    
                    cell.outlinedTextField.rx.text.orEmpty
                        .map { String($0.prefix(element.maxLength ?? 1000)) }
                        .bind(to: cell.outlinedTextField.rx.text)
                        .disposed(by: cell.disposeBag)
                    
                    return cell
                    
                case .outliendTextField:
                    
                    let cell = TextFieldCell.setupText(indexPath: indexPath, tableView: tableView, element: element)
                    
                    cell.outlinedTextField
                        .rx.textInput.text.orEmpty
                        .map{ ($0, element) }
                        .bind(to: self.viewModel.textChangedObs)
                        .disposed(by: cell.disposeBag)
                    
                    cell.outlinedTextField.rx.controlEvent([.editingDidEnd, .editingChanged])
                        .map{ ($0, element) }
                        .asObservable().subscribe({ [weak self] obs in
                            guard let self = self else { return }
                            
                            if self.viewModel.isValidString(text: cell.outlinedTextField.text , regex: obs.element?.1.regex ?? "^[a-zA-Z0-9._-]{1,30}$") {
                                print("done")
                                
                                cell.outlinedTextField.setOutlineColor(self.theme!.outlineColorOnNormal, for: .normal)
                                cell.outlinedTextField.setOutlineColor(self.theme!.outlineColorOnEditing, for: .editing)
                                cell.outlinedTextField.setOutlineColor(self.theme!.outlineColorOnDisabled , for: .disabled)
                                cell.outlinedTextField.leadingAssistiveLabel.text = nil
                                
                            }else{
                                print("ma done")
                                cell.outlinedTextField.leadingAssistiveLabel.text = obs.element?.1.helperLabel
                                
                            }
                        }).disposed(by: cell.disposeBag)
                    
                    cell.outlinedTextField.rx.text.orEmpty
                        .map { String($0.prefix(element.maxLength ?? 1000)) }
                        .bind(to: cell.outlinedTextField.rx.text)
                        .disposed(by: cell.disposeBag)
                    
                    cell.outlinedTextField.keyboardType = .default
                    
                    return cell
                    
                case .normalTextField:
                    
                    let cell = NormalTextFieldCell.setupText(indexPath: indexPath, tableView: tableView, element: element)
                    
                    cell.filledTextField
                        .rx.textInput.text.orEmpty
                        .map{ ($0, element) }
                        .bind(to: self.viewModel.textChangedObs)
                        .disposed(by: cell.disposeBag)
                    cell.filledTextField.keyboardType = .default
                    
                    return cell
                    
                case .selection:
                    
                    let cell = PickerCell.setupPicker(indexPath: indexPath, tableView: tableView, element: element)
                    Observable.just(element.lockups ?? [LookupArrayModel]())
                        .asDriver(onErrorJustReturn: [LookupArrayModel]())
                        .drive(cell.pickerView.rx.itemTitles) { _, item in
                            return item.name ?? ""
                        }.disposed(by: cell.disposeBag)
                    
                    cell.pickerView.rx
                        .itemSelected
                        .asObservable()
                        .subscribe(
                            onNext:{ [weak self] selecteIndex in
                                guard let self = self else { return }
                                
                                let selectedLockup = element.lockups?[selecteIndex.row]
                                cell.pickerTextField.text = selectedLockup?.name ?? ""
                                cell.pickerTextField.setOutlineColor(self.theme!.outlineColorOnNormal, for: .normal)
                                cell.pickerTextField.setOutlineColor(self.theme!.outlineColorOnEditing, for: .editing)
                                cell.pickerTextField.setOutlineColor(self.theme!.outlineColorOnDisabled , for: .disabled)
                                cell.pickerTextField.leadingAssistiveLabel.text = nil
                                self.viewModel.selectedLockupModel.onNext((selectedLockup, index))
                                
                            })
                        .disposed(by: cell.disposeBag)
                    
                    cell.pickerTextField.rx.controlEvent([.editingDidEnd])
                        .map{ ($0) }
                        .asObservable().subscribe({ [weak self] _ in
                            guard let self = self else { return }
                            self.delegate?.selectedSelection(key: element.lookupUserInput?.key ?? "", name: element.lookupUserInput?.name ?? "", index: index)
                        }).disposed(by: cell.disposeBag)
                    
                    return cell
                    
                case .clickableSelection:
                    
                    let cell = ClickableSelectionCell.setupClickableSelection(indexPath: indexPath, tableView: tableView, element: element)
                    Observable.just(element.lockups ?? [LookupArrayModel]())
                        .asDriver(onErrorJustReturn: [LookupArrayModel]())
                        .drive(cell.pickerView.rx.itemTitles) { _, item in
                            return item.name ?? ""
                        }.disposed(by: cell.disposeBag)
                    
                    cell.pickerView.rx
                        .itemSelected
                        .asObservable()
                        .subscribe(
                            onNext:{ [weak self] selecteIndex in
                                guard let self = self else { return }
                                
                                let selectedLockup = element.lockups?[selecteIndex.row]
                                cell.pickerTextField.text = selectedLockup?.name ?? ""
                                cell.pickerTextField.setOutlineColor(self.theme!.outlineColorOnNormal, for: .normal)
                                cell.pickerTextField.setOutlineColor(self.theme!.outlineColorOnEditing, for: .editing)
                                cell.pickerTextField.setOutlineColor(self.theme!.outlineColorOnDisabled , for: .disabled)
                                cell.pickerTextField.leadingAssistiveLabel.text = nil
                                self.viewModel.selectedLockupModel.onNext((selectedLockup, index))
                                
                            })
                        .disposed(by: cell.disposeBag)

                    cell.clickableButton.rx
                        .tap
                        .subscribe(
                            onNext:{ [weak self] _ in
                                        guard let self = self else { return }
                                self.delegate?.clickableSelectionTapped(index: index)
                            })
                        .disposed(by: cell.disposeBag)
                    
                    return cell
                    
                case .date:
                    
                    let cell = DatePickerCell.setupDate(indexPath: indexPath, tableView: tableView, element: element)
                    
                    let datePickerView  : UIDatePicker = UIDatePicker()
                    datePickerView.datePickerMode = UIDatePicker.Mode.date
                    if #available(iOS 14.0, *) {
                        datePickerView.preferredDatePickerStyle = UIDatePickerStyle.inline
                        
                    }
                    
                    datePickerView.autoresizingMask = .flexibleRightMargin
                    datePickerView.rx.value.changed.asObservable()
                        .subscribe({ [weak self] event in
                            guard let self = self else { return }
                            
                            if let date = event.element{
                                let theFormatter = DateFormatter()
                                theFormatter.dateStyle = .medium
                                let theString = theFormatter.string(from: date)
                                cell.datePickerTextField.text = theString
                                self.viewModel.selectedDate.onNext((date, index))
                            }
                        })
                        .disposed(by: cell.disposeBag)
                    
                    cell.datePickerTextField.inputView = datePickerView
                    return cell
                    
                case .attachement:
                    let cell = FileCell.setupAttachment(indexPath: indexPath, tableView: tableView, element: element)
                    cell.importImageButton.rx.tap
                        .flatMapLatest { [weak self] _ in
                            return UIImagePickerController.rx.createWithParent(self) { picker in
                                picker.sourceType = .photoLibrary
                                picker.allowsEditing = false
                            }
                            .flatMap {
                                $0.rx.didFinishPickingMediaWithInfo
                            }
                            .take(1)
                        }
                        .map { info in
                            return info[.originalImage] as? UIImage
                        }
                        .map{ ($0,index) }
                        .bind(to: self.viewModel.selectedAttachement)
                        .disposed(by: cell.disposeBag)
                    
                    return cell
                    
                    
                    
                case .text:
                    let cell = TextViewCell.setupText(indexPath: indexPath, tableView: tableView, element: element)
                    return cell
                    
                case .view:
                    let cell = ViewCell.setupView(indexPath: indexPath, tableView: tableView, element: element)
                    return cell
                    
                case .switchView:
                    let cell = SwitchCell.setupSwitch(indexPath: indexPath, tableView: tableView, element: element)
                    
                    cell.mySwitch.rx
                        .controlEvent(.valueChanged)
                        .withLatestFrom(cell.mySwitch.rx.value)
                        .subscribe(onNext : { bool in
                            element.switchUserSelected?.setOn(bool, animated: true)
                        })
                        .disposed(by: cell.disposeBag)
                    return cell
                    
                case .clickableView:
                    let cell = ClickableViewCell.setupClickableView(indexPath: indexPath, tableView: tableView, element: element)
                    
                    cell.rightButton.rx.tap
                        .bind { [weak self] _ in
                            guard let self = self else { return }
                            
                            self.delegate?.componentCallBack(rootViewController: self, field: element)
                            
                        }.disposed(by: cell.disposeBag)
                    
                    
                    return cell
                    
                case .horizontallyText:
                    let cell = HorizontallyTextCell.setupHorizontallyText(indexPath: indexPath, tableView: tableView, element: element)
                    return cell
                    
                case .none:
                    return UITableViewCell()
                }
            }
            .disposed(by: self.disposeBag)
        
        submitButton.rx
            .tap
            .asDriver(onErrorJustReturn: ())
            .drive(viewModel.submitButtonTapped)
            .disposed(by: disposeBag)
        
        submitButton.rx
            .tap
            .subscribe(
                onNext:{ _ in
                    self.tableView.reloadData()
                })
            .disposed(by: disposeBag)
        
        viewModel.selectedAttachement
            .subscribe(
                onNext:{ _ in
                    self.tableView.reloadData()
                })
            .disposed(by: disposeBag)
        
    }
    
    func setupTheme(){
        
        let theme  = Theme.shared.theme
        submitButton.backgroundColor = theme?.primaryColor
        submitButton.setTitleColor(theme?.buttonTextColor, for: .normal)
        submitButton.layer.cornerRadius = 5
    }
    
    func setupFont(){
        let font = Font.shared.font
        submitButton.titleLabel?.font = font?.textButtonFont
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}

extension FormFieldVC: UITableViewDelegate{
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        for field in self.viewModel.fieldsArray{
            if field.type == .view{
                return UITableView.automaticDimension
            }else{
                return 80
            }
        }
        return 80
    }
}
