//
//  LookupModel.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 13/03/2022.
//

import Foundation

// MARK: - SystemLookup
public class LookupModel {
    let id: String?
   public let array: [LookupArrayModel]?
    
    public init(id: String?, array: [LookupArrayModel]?){
        self.id = id
        self.array = array
    }
}
// MARK: - Array
public class LookupArrayModel {
    public let key, name: String?
    
    public init(key: String?, name: String?){
        self.key = key
        self.name = name
    }
}
