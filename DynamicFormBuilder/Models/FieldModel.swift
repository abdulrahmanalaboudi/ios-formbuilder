//
//  FieldModel.swift
//  DynamicFormBuilder
//
//  Created by Homam Abusaif on 28/02/2022.
//


import Foundation
import UIKit

// MARK: - Field
@objc public class FieldModel: NSObject {
    public var name, label, secondaryLabel, helperLabel, placeholder: String?
    public var type: FormFieldType?
    public var fieldRequired: Bool?
    public var lookupKey: String?
    public var attachment: [FieldModel]?
    public var stringUserInput: String? = ""
    public var lockups: [LookupArrayModel]?
    public var floatUserInput: Float? = 0.0
    public var lookupUserInput: LookupArrayModel? = LookupArrayModel(key: nil, name: nil)
    public var dateUserInput: Date? = Date()
    public var imageUserInput: UIImage?
    public var switchUserSelected: UISwitch? = UISwitch()
    public var leftImageView: UIImage?
    public var rightImageView: UIImage?
    public var cornerRadius: CGFloat? = 0.0
    public var regex: String?
    public var isValid: Bool?
    public var maxLength: Int?
    
    public init(name: String, label: String, secondaryLabel: String? = nil, helperLabel:  String? = nil, placeholder: String? = nil, type: FormFieldType, lockups: [LookupArrayModel]?, fieldRequired: Bool = false, lookupKey: String? = nil, floatUserInput: Float? = 0.0, imageUserInput: UIImage? = nil, switchUserSelected: UISwitch? = nil, stringUserInput: String?, lookupUserInput: LookupArrayModel? = nil, dateUserInput: Date? = nil, leftImageView: UIImage? = nil, rightImageView: UIImage? = nil, cornerRadius: CGFloat? = 0.0, regex: String? = nil, isValid: Bool? = true, maxLength: Int? = 100000){
        self.name = name
        self.label = label
        self.type = type
        self.lockups = lockups
        self.fieldRequired = fieldRequired
        self.stringUserInput = stringUserInput
        self.lookupUserInput = lookupUserInput
        self.dateUserInput = dateUserInput
        self.lookupKey = lookupKey
        self.floatUserInput = floatUserInput
        self.imageUserInput = imageUserInput
        self.switchUserSelected = switchUserSelected
        self.secondaryLabel = secondaryLabel
        self.helperLabel = helperLabel
        self.leftImageView = leftImageView
        self.rightImageView = rightImageView
        self.placeholder = placeholder
        self.cornerRadius = cornerRadius
        self.regex = regex
        self.isValid = isValid
        self.maxLength = maxLength
    }
}

public enum FormFieldType: String {
    case date = "Date"
    case selection = "Selection"
    case outliendTextField = "String"
    case attachement = "attachement"
    case float = "Float"
    case text = "Text"
    case normalTextField = "NormalTextField"
    case view = "View"
    case switchView = "Switch"
    case clickableView = "ClickableView"
    case horizontallyText = "HorizontallyText"
    case clickableSelection = "ClickableSelection"
}
